""" sample code for demo"""
import unittest

def add2(count_1:int, count_2:int)->int:
    """sum two numbers"""
    print("count_1: ", count_1, "count_2: ", count_2)
    return count_1 + count_2


# unit test class
class TestSum(unittest.TestCase):
    """unit test class"""
    def test_sum(self):
        """unit test function"""
        self.assertEqual(add2(1, 2), 3)
        self.assertEqual(add2(1, 3), 4)

if __name__ == '__main__':
    unittest.main()
